# 3. Impression 3D

Le jeudi 15 octobre, j'ai assisté à la formation sur les impressions 3D. Elle a commencé par une présentation du logiciel compatible avec les imprimantes 3D du fablab: PrusaSlicer. Ayant déjà utilisé le logiciel Cura compatible avec les imprimantes Ultimaker, j’ai vite trouvé mes marques grâce aux nombreuses similitudes entre les deux logiciels.


## Processus de recherche : erreurs et succès

**1. Logiciel - PrusaSlicer**

Après avoir modélisé mon dessin avec Fusion360 (explications voir [module 2](https://gitlab.com/louise.vandeneynde/firstname.surname/-/blob/master/docs/modules/module02.md)), je l'ai exporté au format STL pour pouvoir l'ouvrir sur le logiciel PrusaSlicer 
![](docs/images/M3.1.png)

Après cela, j'ai ouvert le fichier dans PrusaSlicer en suivant les étapes 1, 2 et 3. Une fois ouvert, le fichier était trop grand, je l'ai donc réduit, grâce à l'outil échelle, en diminuant la diagonale. 
![](docs/images/M3.2.png)

Une fois le modèle réduit, j'ai été dans les réglages d'impression. Là, j'y ai modifié le nombre de couches verticales et horizontales minimum, le motif de remplissage et j'ai généré des supports. Les modifications sont reconnaissables là où le cadenas est orange. 
![](docs/images/M3.3.png)

Une fois ces modifications effectuées, je suis retournée dans le plateau et j'ai vérifié que les supports avaient bien été générés (1). Voyant que tout était bon, j'ai exporté le G-code en appuyant sur le bouton du même nom en bas à droite (2).
![](docs/images/M3.4.png)

Enfin, j'ai mis le fichier sur une carte SD dans un dossier à mon nom et je l'ai placée dans l'imprimante 3D.


---


**2. Imprimante 3D - PRUSA I3MK3S**

La carte SD est à placer sur le côté gauche du "tableau de bord". Il faut faire attention à l'imprimante qu'on utilise. Celle que j'ai utilisée a, comme indiqué sur l'image par un cercle rouge, une buse de 6mm. Cela signifie qu'il faut changer, dans les paramètres d'impression, la hauteur de couche et la hauteur de la première couche de 2mm (utilisée pour les imprimantes avec une buse de 4mm) à 3mm. En effet, la hauteur de couche doit être égale à la moitié de la taille de la buse. Il faut aussi faire attention à ce que le plateau aimanté soit bien placé horiontalement (si ce n'est pas le cas, l'imprimante va faire un calibrage inintérrompu de l'axe Z) et le nettoyer avec de l'acétone.
![](docs/images/M3.1bis-ConvertImage.png)

 Il se peut, comme moi, que l'on veuille changer de filament. Pour cela, la marche à suivre est la suivante: 
d'abord, il faut trouver, dans le menu déroulant l'action "Unload filament" qui permet de retirer le fimament. La buse chauffe et cela permet de tirer légérement sur le filament pour l'extraire. Une fois extrait, il faut couper le bout pour le ranger dans un trou de la bobine. Une fois rangé, on peut mettre le nouveau filament. Pour cela, il faut le mettre sur la machine puis dans le menu déroulant, appuyer sur "Load filament". Une fois cela fait, on peut mettre le filament. Finalement, la machine demande si le filament extrudé est de la bonne couleur et il faut répondre "non" plusieurs fois afin d'être sûr qu'il n'y ait pas de résidus de l'ancien filament. Cela fait, on peut appuyer sur "oui" et utiliser la machine.
![](docs/images/M3.4bis.png)

Pour lancer l'impression, il faut d'abord trouver son fichier dans la carte SD. Pour cela, il faut aller dans "Print from SD" et appuyer sur le bouton. Puis, il faut trouver son dossier dans la liste; pour cela il se peut que l'on doive utiliser la molette, et, enfin, ouvrir son fichier. 
![](docs/images/M3.2bis.png)

Maintant l'impresion est lancée. Toutefois, il se peut que l'on doive/veuille modifier certains paramètres. Pour cela il faut appuyer sur le bouton et aller dans "Tune". Cela permet de changer:

- la vitesse d'impression - **"Speed"**: par défaut elle est à 100%; il ne faut pas aller plus vite. Ayant raté plusieurs fois mon impression, je suis descendue à 75% pour imprimer les premières couches.

- La température de la buse - **"Nozzel"**: par défaut elle est à 220°. Elle dépend de la température du filament (voir étiquette sur la bobine). Pour le filament que j'ai utilisé, la température conseillée se situait entre 90 et 115° du coup, j'ai descendu la température de la buse à 110°.

- La température du plateau - **"Bed"**: par défaut elle est à 60° mais on m'a conseillée de descendre à 55°, ce que j'ai fait.
![](docs/images/M3.3bis.png)

Pour retirer l'impression il faut retirer le plateau aimanté et le "craquer" dans les deux sens. Voici les résultats obtenus. J'ai arrêté la première impression (image de gauche) car il y a eu un problème de filament: l'ancien filament noir a contaminé l'impression. Après une nouvelle tentative avec le même résulat, j'ai demandé de l'aide aux fabmanageuses (merci!) et je n'ai plus eu de souci. On peut voir le résulat (image de droite) avec encore les supports. 
![](docs/images/M3.5bis-ConvertImage.png)

--- 


**3. Résulat: comparaison entre les deux impressions**

Ceci est une comparaison entre les deux impressions suivant deux imprimantes et modèles Fusion360 différents. L'impression jaune a été réalisée avec l'ancien modèle et une imprimante 4mm et la bleue avec le nouveau modèle et une imprimante 6mm. La bleue, retravaillée, est plus semblable au modèle d'origine mais, à cause des problèmes que j'ai eus avec l'imprimante, je le trouve moins précis que la première impression.
![](docs/images/M3.6bis.png)

